package dk.tornkvist.assignment;


import dk.tornkvist.assignment.solution.SimpleSolution;
import dk.tornkvist.assignment.solution.Solution;
import dk.tornkvist.assignment.solution.SolutionUtility;
import dk.tornkvist.assignment.solution.ThreadSolution;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for solution to count unique words.
 */
public class SolutionTest
{
    private Logger logger = LoggerFactory.getLogger(SolutionTest.class);

    @Test
    public void testCountUniqueWords() {
        String line = "Enter ALONSO, SEBASTIAN, ANTONIO, FERDINAND, GONZALO, and others";
        String pattern = SolutionUtility.createSearchPattern("and");
        int actualCount = SolutionUtility.countUniqueWords(line, pattern);
        int expectedCount = 1;

        assertEquals("Count of uniq words must be " + expectedCount, expectedCount, actualCount);
    }

    /**
     * Testing the number of a word appears in the test file.
     * The result of expected occurrence for the 10 highest appeared words in the text file is inserted to check against.
     *
     * @throws IOException
     */
    @Test
    public void testReadAndCountAllWordsFromFile() throws IOException {
        Map<String,AtomicInteger> expectedAllWordMap = new HashMap<>();
        Map<String,AtomicInteger> actualAllWordMap;

        expectedAllWordMap.put("and", new AtomicInteger(514));
        expectedAllWordMap.put("the", new AtomicInteger(513));
        expectedAllWordMap.put("i", new AtomicInteger(446));
        expectedAllWordMap.put("to", new AtomicInteger(324));
        expectedAllWordMap.put("a", new AtomicInteger(310));
        expectedAllWordMap.put("of", new AtomicInteger(295));
        expectedAllWordMap.put("my", new AtomicInteger(288));
        expectedAllWordMap.put("you", new AtomicInteger(211));
        expectedAllWordMap.put("that", new AtomicInteger(188));
        expectedAllWordMap.put("this", new AtomicInteger(185));

        Solution solution = new SimpleSolution();

        long elapsedTimeBefore = System.nanoTime();
        actualAllWordMap = solution.readAndCountAllWordsFromFile(App.FILE_LOCATION);
        long elapsedTimeAfter = System.nanoTime();

        logger.info("elapsed time for " + solution.getClass().getName() + ": " + ((elapsedTimeAfter - elapsedTimeBefore)/1000000));

        assertEquals("Number of unique words must be " + expectedAllWordMap.size(), expectedAllWordMap.size(), actualAllWordMap.size());

        for (Map.Entry<String, AtomicInteger> expectedWord : expectedAllWordMap.entrySet()) {
            assertTrue("Key is present in both maps", actualAllWordMap.containsKey(expectedWord.getKey()));
            assertEquals("Count of the unique word \"" + expectedWord.getKey() + "\" in test file must be " + expectedWord.getValue().toString(),
                    expectedWord.getValue().get(),
                    actualAllWordMap.get(expectedWord.getKey()).get()
            );
        }
    }

    /**
     * Testing the number of unique words in the test file. The result of expected occurrence in the text file is inserted to check against.
     * Also a elapsed time for the algoritme tested is also noted.
     *
     * @throws IOException
     */
    @Test
    public void testReadAndCountUniqueWordsFromFile() throws IOException {
        Map<String,AtomicInteger> expectedUniqueWordMap = new HashMap<>();

        expectedUniqueWordMap.put("and", new AtomicInteger(514));
        expectedUniqueWordMap.put("the", new AtomicInteger(513));
        expectedUniqueWordMap.put("i", new AtomicInteger(446));
        expectedUniqueWordMap.put("to", new AtomicInteger(324));
        expectedUniqueWordMap.put("a", new AtomicInteger(310));
        expectedUniqueWordMap.put("of", new AtomicInteger(295));
        expectedUniqueWordMap.put("my", new AtomicInteger(288));
        expectedUniqueWordMap.put("you", new AtomicInteger(211));
        expectedUniqueWordMap.put("that", new AtomicInteger(188));
        expectedUniqueWordMap.put("this", new AtomicInteger(185));

        Solution simpleSolution = new SimpleSolution();

        Solution threadSolution = new ThreadSolution();

        // using BufferedReader seems to run faster after first run so this is why this first run is executed maybe som file is store in RAM.
        readAndCountUniqueWordsFromFileExcecute(simpleSolution, expectedUniqueWordMap);

        readAndCountUniqueWordsFromFileExcecute(simpleSolution, expectedUniqueWordMap);

        readAndCountUniqueWordsFromFileExcecute(threadSolution, expectedUniqueWordMap);
    }

    private void readAndCountUniqueWordsFromFileExcecute(Solution solution, Map<String,AtomicInteger> expectedUniqueWordMap) throws IOException {
        Map<String,AtomicInteger> actualUniqueWordMap = new HashMap<>();

        actualUniqueWordMap.put("and", new AtomicInteger(0));
        actualUniqueWordMap.put("the", new AtomicInteger(0));
        actualUniqueWordMap.put("i", new AtomicInteger(0));
        actualUniqueWordMap.put("to", new AtomicInteger(0));
        actualUniqueWordMap.put("a", new AtomicInteger(0));
        actualUniqueWordMap.put("of", new AtomicInteger(0));
        actualUniqueWordMap.put("my", new AtomicInteger(0));
        actualUniqueWordMap.put("you", new AtomicInteger(0));
        actualUniqueWordMap.put("that", new AtomicInteger(0));
        actualUniqueWordMap.put("this", new AtomicInteger(0));

        long elapsedTimeBefore = System.nanoTime();
        actualUniqueWordMap = solution.readAndCountUniqueWordsFromFile(App.FILE_LOCATION, actualUniqueWordMap);
        long elapsedTimeAfter = System.nanoTime();

        logger.info("elapsed time for " + solution.getClass().getName() + ": " + ((elapsedTimeAfter - elapsedTimeBefore)/1000000));

        assertEquals("Number of unique words must be " + expectedUniqueWordMap.size(), expectedUniqueWordMap.size(), actualUniqueWordMap.size());

        for (Map.Entry<String, AtomicInteger> expectedUniqueWord : expectedUniqueWordMap.entrySet()) {
            assertTrue("Key is present in both maps", actualUniqueWordMap.containsKey(expectedUniqueWord.getKey()));
            assertEquals("Count of the unique word \"" + expectedUniqueWord.getKey() + "\" in test file must be " + expectedUniqueWord.getValue().toString(),
                    expectedUniqueWord.getValue().get(),
                    actualUniqueWordMap.get(expectedUniqueWord.getKey()).get()
            );
        }
    }

    /**
     * This is a special test to see the different chars around a unique word.
     * This will help giving a picture of the different ways unique word is placed
     * It helps to find the right regex
     */
    @Ignore
    public void testCountOfUniqueWordPlaces() throws IOException {
        Map<String,AtomicInteger> uniqueWordSum = new HashMap<>();
        Map<String,AtomicInteger> uniqueWordSumNew = new HashMap<>();

        String pattern = "\\b[Aa]nd\\b";
        Pattern patternCompile = Pattern.compile(pattern);

        String patternNew = SolutionUtility.createSearchPattern("and");
        Pattern patternCompileNew = Pattern.compile(patternNew);

        InputStream inputStream = App.class.getResourceAsStream(App.FILE_LOCATION);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String line;
        String uniqueWordPlace;

        while((line = bufferedReader.readLine()) != null) {
            Matcher matcher = patternCompile.matcher(line);

            while(matcher.find()) {
                int start = matcher.start() > 0 ? matcher.start() - 1 : 0;
                int end = matcher.end() < line.length() ? matcher.end() + 1 : line.length();

                uniqueWordPlace = line.substring(start,end);

                if(uniqueWordSum.containsKey(uniqueWordPlace)) {
                    uniqueWordSum.get(uniqueWordPlace).incrementAndGet();
                } else {
                    uniqueWordSum.put(uniqueWordPlace, new AtomicInteger(1));
                }
            }

            Matcher matcherNew = patternCompileNew.matcher(line);

            while(matcherNew.find()) {
                int start = matcherNew.start();
                int end = matcherNew.end();

                uniqueWordPlace = line.substring(start,end);

                if(uniqueWordSumNew.containsKey(uniqueWordPlace)) {
                    uniqueWordSumNew.get(uniqueWordPlace).incrementAndGet();
                } else {
                    uniqueWordSumNew.put(uniqueWordPlace, new AtomicInteger(1));
                }
            }
        }

        bufferedReader.close();

        AtomicInteger total = new AtomicInteger(0);
        logger.info("--- Pattern ------------");
        uniqueWordSum.forEach((k,v)->logger.info("Place: " + k + " Count: " + v));
        uniqueWordSum.forEach((k,v)->total.addAndGet(v.get()));
        logger.info("total: " + total.get());

        AtomicInteger totalNew = new AtomicInteger(0);
        logger.info("--- Pattern new ---------");
        uniqueWordSumNew.forEach((k,v)->logger.info("Place: " + k + " Count: " + v));
        uniqueWordSumNew.forEach((k,v)->totalNew.addAndGet(v.get()));
        logger.info("total: " + totalNew.get());
    }
}
