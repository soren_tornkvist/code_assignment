package dk.tornkvist.assignment.solution;

import java.io.*;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class have a multi thread solution of the code assignment to count unique words.
 */
public class ThreadSolution implements Solution {

    @Override
    public Map<String, AtomicInteger> readAndCountAllWordsFromFile(String path) throws IOException {
        return null;
    }

    @Override
    public Map<String, AtomicInteger> readAndCountUniqueWordsFromFile(String path, Map<String, AtomicInteger> uniqueWordList) throws IOException {
        ExecutorService executorService = Executors.newFixedThreadPool(20);

        InputStream inputStream = getClass().getResourceAsStream(path);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String line;

        while((line = bufferedReader.readLine()) != null) {
            for (Map.Entry<String, AtomicInteger> uniqueWord : uniqueWordList.entrySet()) {
                Runnable countALine = new CountALine(line, uniqueWord.getKey(), uniqueWordList.get(uniqueWord.getKey()));
                executorService.execute(countALine);
            }
        }

        bufferedReader.close();

        executorService.shutdown();

        try {
            executorService.awaitTermination(10,TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return uniqueWordList;
    }

    private class CountALine implements Runnable {
        private String line;
        private String uniqueWord;
        private AtomicInteger uniqueWordCount;

        public CountALine(String line, String uniqueWord, AtomicInteger uniqueWordCount) {
            this.line = line;
            this.uniqueWord = uniqueWord;
            this.uniqueWordCount = uniqueWordCount;
        }

        @Override
        public void run() {
            int numberOfUniqueWords =  SolutionUtility.countUniqueWords(line,SolutionUtility.createSearchPattern(uniqueWord));
            uniqueWordCount.addAndGet(numberOfUniqueWords);
        }
    }
}
