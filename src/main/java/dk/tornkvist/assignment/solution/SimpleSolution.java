package dk.tornkvist.assignment.solution;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class have a one thread solution of the code assignment to count unique words.
 */
public class SimpleSolution implements Solution {

    @Override
    public Map<String, AtomicInteger> readAndCountAllWordsFromFile(String path) throws IOException {
        Map<String,AtomicInteger> wordMap = new HashMap<>();

        InputStream inputStream = getClass().getResourceAsStream(path);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String line;

        Pattern patternCompile = Pattern.compile(SolutionUtility.FIND_UNIQUE_WORDS_REGEX);

        while((line = bufferedReader.readLine()) != null) {
            Matcher matcher = patternCompile.matcher(line);
            String word;

            while (matcher.find()) {
                word = matcher.group();
                word = word.toLowerCase(Locale.ENGLISH);

                if (wordMap.containsKey(word)) {
                    wordMap.get(word).incrementAndGet();
                } else {
                    wordMap.put(word, new AtomicInteger(1));
                }

            }
        }



        return SolutionUtility.sortWordMapTrimSize(wordMap,10);
    }

    public Map<String,AtomicInteger> readAndCountUniqueWordsFromFile(String path, Map<String,AtomicInteger> uniqueWordList) throws IOException {
        InputStream inputStream = getClass().getResourceAsStream(path);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String line;

        while((line = bufferedReader.readLine()) != null) {
            for (Map.Entry<String, AtomicInteger> uniqueWord : uniqueWordList.entrySet()) {
                int numberOfUniqueWords = SolutionUtility.countUniqueWords(line,SolutionUtility.createSearchPattern(uniqueWord.getKey()));
                uniqueWordList.get(uniqueWord.getKey()).addAndGet(numberOfUniqueWords);
            }
        }

        bufferedReader.close();

        return uniqueWordList;
    }
}
