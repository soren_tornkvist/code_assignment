package dk.tornkvist.assignment.solution;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class for different solutions to count unique words.
 */
public class SolutionUtility {
    // Regex for finding all words
    public final static String FIND_UNIQUE_WORDS_REGEX = "(?<=(([\\s\\[])|^))([A-Za-z]+)(?=(([\\s\\.,\\?\\!:;\\]])|$))";

    /**
     * Sort a map after highest value and trim down size on map
     *
     * @param wordMap   a unsorted map
     * @param trim      a number the map should be trim to in size.
     * @return          a new sortedmap
     */
    public static Map<String, AtomicInteger> sortWordMapTrimSize(Map<String, AtomicInteger> wordMap, int trim) {
        List<Map.Entry<String, AtomicInteger>> wordList = new LinkedList<>(wordMap.entrySet());

        Collections.sort(wordList, (o1, o2) -> (new Integer(o2.getValue().get()).compareTo(o1.getValue().get())));

        Map<String, AtomicInteger> sortedWordMap = new LinkedHashMap<>();
        int count = 0;

        for(Map.Entry<String, AtomicInteger> wordEntry : wordList) {
            sortedWordMap.put(wordEntry.getKey(), wordEntry.getValue());
            count++;
            if(count >= trim) {
                break;
            }
        }

        return sortedWordMap;
    }

    /**
     * Return the number of times pattern is found in the line
     *
     * @param line      a string containing text to search in. Must not be null.
     * @param pattern   a string containing the search pattern. Must not be null
     * @return          the number of times pattern is found in the line
     */
    public static int countUniqueWords(String line, String pattern) {
        if(line == null) {
            throw new NullPointerException("line is not defined");
        }
        if(pattern == null) {
            throw new NullPointerException("pattern is not defined");
        }

        int count = 0;

        Pattern patternCompile = Pattern.compile(pattern);

        Matcher matcher = patternCompile.matcher(line);

        while(matcher.find()) {
            count++;
        }

        return count;
    }

    /**
     * Creating a regex string for a unique word.
     * the unique word will be uncased sensitive on the first char.
     * Char before unique word is whitespace(\s) or square brackets(\[) or it is beginning of line(^)
     * Char after unique word is whitespace(\s) or dot(\.) or comma(,) or questionmark(\?) or exclamation(\!) or colon(:) or semicolon(;) or square brackets(\]) or it is end of line($)
     *
     * @param uniqueWordList    a string containing a unique word. Must not be null or empty
     * @return                  a string containing a regex pattern for searching
     */
    public static String createSearchPattern(String uniqueWordList) {
        if(uniqueWordList == null) {
            throw new NullPointerException("Unique word is not defined");
        }
        if(uniqueWordList.length() <= 0) {
            throw new IllegalArgumentException("Unique word is empty");
        }

        // regex group in front of unique word. Search for char before is whitespace(\s) or square brackets(\[) or it is beginning of line(^)
        String regexFront = "(([\\s\\[])|^)";

        // regex group in front of unique word. Search for char after is whitespace(\s) or dot(\.) or comma(,) or questionmark(\?) or exclamation(\!) or colon(:) or semicolon(;) or square brackets(\]) or it is end of line($)
        String regexback = "(([\\s\\.,\\?\\!:;\\]])|$)";

        StringBuilder pattern = new StringBuilder(32);

        pattern.append(regexFront);
        pattern.append('[');
        pattern.append(Character.toUpperCase(uniqueWordList.charAt(0)));
        pattern.append(Character.toLowerCase(uniqueWordList.charAt(0)));
        pattern.append(']');

        for (int i = 1; i < uniqueWordList.length(); i++){
            pattern.append(Character.toLowerCase(uniqueWordList.charAt(i)));
        }

        pattern.append(regexback);

        return pattern.toString();
    }
}
