package dk.tornkvist.assignment.solution;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Interface for diffrent solution of the code assignment to count unique words.
 */
public interface Solution {

    /**
     * It will count all the words in the file and return the number of appears for each word.
     *
     * @param path              a string containing the path for the text file to be search in.
     * @return                  the number of times a words exits in the given file for all words
     * @throws IOException
     */
    Map<String,AtomicInteger> readAndCountAllWordsFromFile(String path) throws IOException;

    /**
     * It will count all the unique words in the file and return the number for each unique word.
     *
     * @param path              a string containing the path for the text file to be search in.
     * @param uniqueWordList    a map containing all the unique words
     * @return                  the number of times the unique words exits in the given file for each unique word
     * @throws IOException
     */
    Map<String,AtomicInteger> readAndCountUniqueWordsFromFile(String path, Map<String,AtomicInteger> uniqueWordList) throws IOException;
}
