package dk.tornkvist.assignment;

import dk.tornkvist.assignment.solution.SimpleSolution;
import dk.tornkvist.assignment.solution.Solution;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Main App
 */
public class App
{
    public static final String FILE_LOCATION = "/tempest.txt";

    public static void main( String[] args ) throws IOException {
        System.out.println("start");

        Solution solution = new SimpleSolution();
        Map<String,AtomicInteger> wordMap;

        try {
            wordMap = solution.readAndCountAllWordsFromFile(FILE_LOCATION);
            wordMap.forEach((k,v)->System.out.println("Word: " + k + " Count: " + v));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
